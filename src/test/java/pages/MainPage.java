package pages;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;

public class MainPage extends BasePage {

    @Step("Open main page via url")
    public void open() {
        Selenide.open("");
    }
}
