package pages;

import io.qameta.allure.Step;

public class BasePage {

    //This page is for methods and elements that are similar for ALL pages

    @Step("Test step")
    public void testAction() {
//        $("");    == driver.findElement(By.cssSelector(""));
//        $x("");   == driver.findElement(By.xpath(""));
//        $$("");   == driver.findElements(By.cssSelector(""));
//        $$x("");  == driver.findElements(By.xpath(""));
//        You can also use $(By.someSelector("")); if you want other selector
    }
}
