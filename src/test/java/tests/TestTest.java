package tests;

import io.qameta.allure.*;
import org.testng.annotations.Test;

@Owner("Test owner")
public class TestTest extends BaseTest {

    @Test(description = "Test description1")
    @Feature("Test feature1")
    @Story("Test story1")
    @Tags({"testTag"})
    @Severity(SeverityLevel.MINOR)
    public void Test1() {

    }

    @Test(description = "Test description2")
    @Feature("Test feature2")
    @Story("Test story2")
    @Tags({"testTag"})
    @Severity(SeverityLevel.BLOCKER)
    public void Test2() {

    }

    @Test(description = "Test description3")
    @Feature("Test feature3")
    @Story("Test story3")
    @Tags({"testTag"})
    @Severity(SeverityLevel.NORMAL)
    public void Test3() {

    }
}
