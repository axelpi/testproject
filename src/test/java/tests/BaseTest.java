package tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.BasePage;
import pages.MainPage;
import utils.PropertyReader;

public class BaseTest {

    public MainPage mainPage = new MainPage();
    public BasePage basePage = new BasePage();

    @BeforeMethod
    public void setup() {
        Configuration.timeout = 30000;
        Configuration.browser = "chrome";
        Configuration.baseUrl = PropertyReader.getProperty("test.property.url");
        Configuration.startMaximized = true;
        Configuration.headless = false;
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
    }

    @AfterMethod
    public void closeDriver() {
        WebDriverRunner.closeWebDriver();
    }
}
